CREATE TABLE Categoria(
    ID int identity(1,1) primary key,
    nombre varchar(100) not null
)

CREATE TABLE Usuario(
    ID int identity(1,1) primary key,
    nombre varchar(100) not null,
)

CREATE TABLE Post(
    ID int identity(1,1) primary key,
    titulo varchar(200) not null,
    pubDate DateTime not null,
    enlace varchar(300) not null,
    descripcion varchar(max) not null,
    contenido varchar(max) not null,
    ID_Usuario int not null,
    CONSTRAINT FK_ID_Usuario FOREIGN KEY (ID_Usuario) REFERENCES Usuario(ID),
)

CREATE TABLE CategoriasPosts(
    ID_Post int not null,
    ID_Categoria int not null,
    CONSTRAINT FK_ID_Post FOREIGN KEY (ID_Post) REFERENCES Post(ID),
    CONSTRAINT FK_ID_Categoria FOREIGN KEY (ID_Categoria) REFERENCES Categoria(ID)
)

insert into Categoria (nombre) values ('Categoría por defecto')

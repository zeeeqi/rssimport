package com.cesur;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.SAXException;

public class XmlParse {

    public List<String> leer(String ruta_xpath, String url_) {
        List<String> list = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        try {
            URL url = new URL(url_);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String entrada;
            String cadena = "";
            while ((entrada = br.readLine()) != null) {
                cadena = cadena + entrada;
            }
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource archivo = new InputSource();
            archivo.setCharacterStream(new StringReader(cadena));
            Document doc = db.parse(archivo);
            // doc.getDocumentElement().normalize();

            // Create XPathFactory object
            XPathFactory xpathFactory = XPathFactory.newInstance();

            // Create XPath object
            XPath xpath = xpathFactory.newXPath();

            XPathExpression expr = xpath.compile(ruta_xpath);

            NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++)
                list.add(nodes.item(i).getNodeValue());
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Post> leerTodo(String url)
            throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
            
        List<Post> todosLosPosts = new ArrayList<Post>();
        try {
            
            // La expresion xpath de busqueda
            String xPathExpression = "//item";

            // Carga del documento xml
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(url);
            
            // Preparación de xpath
            XPath xpath = XPathFactory.newInstance().newXPath();

            // Consultas
            NodeList itemList = (NodeList) xpath.evaluate(xPathExpression, documento, XPathConstants.NODESET);
           // NodeList prueba = documento.getElementsByTagName("item");
           DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss +0000").withLocale(Locale.ENGLISH);

            for (int i = 0; i < itemList.getLength(); i++) {
                Post post = new Post();
                NodeList hijosList = itemList.item(i).getChildNodes();
                for (int j = 0; j< hijosList.getLength();j++){
                    
                    Node x = hijosList.item(j);
                    if (x.getNodeType() == 1){
                        String valor = x.getTextContent();
                        if (valor == null) valor = "No se pudo leer ese campo";
                        switch (x.getNodeName()){
                            
                            case "title":
                                post.setTitulo(valor);
                                break;
                            case "link":
                                post.setEnlace(valor);
                                break;
                            case "description":
                                post.setDescripcion(valor);
                                break;
                            case "pubDate":
                                post.setPubDate(LocalDateTime.parse(valor, formatter));
                                break;
                            case "category":
                                post.addCategoria(valor);
                                break;
                            case "content:encoded":
                                post.setContenido(valor);
                                break;
                            case "dc:creator":
                                post.setAutor(valor);
                            default:
                                break;
                        }

                    } 
                }
                todosLosPosts.add(post);
            }
        } catch (ParserConfigurationException | IOException | XPathExpressionException e) {
            e.printStackTrace();
        } catch (SAXException e){
            System.out.println("La URL debe ser un feed");
            System.out.println(e.getMessage());
        }

        return todosLosPosts;
    }

}
package com.cesur;

import java.io.IOException;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

public class Menu {

    public static void menuPrincipal() throws IOException, SQLException, XPathExpressionException, ParserConfigurationException, SAXException {
		int opc;
		do {
			System.out.println("------------MENU PRINCIPAL------------");
			System.out.println("1.- Crear la BBDD");
			System.out.println("2.- Insertar Feed a la BBDD");
			System.out.println("3.- Ver últimos 10 posts");
			System.out.println("4.- Ver todos los posts.");
			System.out.println("5.- Ver un post en detalle.");
			System.out.println("6.- Buscar posts por titulo");
            System.out.println("7.- Buscar posts por contenido");
            System.out.println("8.- Filtrar por categoría");		
			System.out.println("9.- Salir.");
			opc = pedirNumeroEntre(1, 9);

			switch (opc) {
			case 1:
				App.CrearBBDD();
				break;
			case 2:
		        App.InsertarFeedBBDD();
                break;
			case 3:
                App.mostrarUltimos10Post();
				break;
			case 4:
				App.mostrarTodosLosPost();
				break;
			case 5:
                App.verUnPostEnDetalle();
				break;
			case 6:
                App.buscarPorTitulo();
				break;
			case 7:
                App.buscarPorContenido();
                break;
            case 8:
                App.filtrarPorCategoria();
                break;
            default:
				System.out.println("Muchas gracias.");
			}
		} while (opc != 9);

	}

    public static int pedirNumeroEntre(int desde, int hasta) {
		int num = 0;
		Scanner sc = new Scanner(System.in);
		try {

			do {
				num = sc.nextInt();
				if (num < desde || num > hasta) {
					System.out.println("Debe estar entre " + desde + " y " + hasta);
				}
			} while (num < desde || num > hasta);
 
		} catch (InputMismatchException ex) {
			System.out.println("Debes ingresar obligatoriamente un número.");
			sc.nextLine();
		}

		return num;

	}

    
}

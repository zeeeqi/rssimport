package com.cesur;

import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App {
    private static String serverDB = "localhost";
    private static String portDB = "1533";
    private static String DBname = "rssFeed";
    private static String userDB = "sa";
    private static String passwordDB = "12345Ab##";

    /**
     * @param args
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws XPathExpressionException
     * @throws SQLException
     */
    public static void main(String[] args) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException, SQLException {


        Menu.menuPrincipal();
    }

    public static void filtrarPorCategoria()throws SQLException{
        Scanner sc = new Scanner(System.in);
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        ResultSet rs = b.hacerConsulta("Select * from categoria");
        b.imprimirConsultaPorConsola(rs);

        System.out.println("Elige la categoría que quieres ver.");
        int idCategoria = sc.nextInt();
        sc.nextLine();

        rs = b.hacerConsulta("select p.id,p.titulo from post p inner join CategoriasPosts cp on cp.ID_Post = p.ID where cp.ID_Categoria ="+idCategoria);
        b.imprimirConsultaPorConsola(rs);

    }

    public static void buscarPorContenido() throws SQLException{
        Scanner sc = new Scanner(System.in);
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        System.out.println("Introduce la palabra por la que quieres filtrar");
        String filtro = sc.nextLine();

        ResultSet rs = b.hacerConsulta("Select id,titulo from post where contenido like '%"+filtro+"%'");

        b.imprimirConsultaPorConsola(rs);


    }

    public static void buscarPorTitulo() throws SQLException{
        Scanner sc = new Scanner(System.in);
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        System.out.println("Introduce la palabra por la que quieres filtrar");
        String filtro = sc.nextLine();

        ResultSet rs = b.hacerConsulta("Select id,titulo from post where titulo like '%"+filtro+"%'");

        b.imprimirConsultaPorConsola(rs);

    }

    public static void mostrarUltimos10Post() throws SQLException{
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        ResultSet rs = b.hacerConsulta("Select top 10 id,pubdate,titulo from post order by pubdate desc");
        b.imprimirConsultaPorConsola(rs);

    }

    public static void mostrarTodosLosPost() throws SQLException{
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        ResultSet rs = b.hacerConsulta("Select id,pubdate,titulo from post");
        b.imprimirConsultaPorConsola(rs);

    }

    public static void verUnPostEnDetalle() throws SQLException{
        Scanner sc = new Scanner(System.in);
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        System.out.println("Introduce el ID del Post que quieres ver ");
        int idPost = sc.nextInt();

        ResultSet rs = b.hacerConsulta("Select * from post where id = "+idPost);
        b.imprimirConsultaPorConsola(rs);

        rs = b.hacerConsulta("Select nombre as Categorias from categoria c inner join categoriasPosts cp on cp.id_categoria = c.id inner join post p on p.id = cp.id_post where cp.ID_Post ="+idPost);  
        b.imprimirConsultaPorConsola(rs);
    }

    public static void InsertarFeedBBDD() throws XPathExpressionException, ParserConfigurationException, SAXException, IOException{
        Scanner sc = new Scanner(System.in);
        XmlParse x = new XmlParse();

        System.out.println("Introduce la URL que deseas importar: ");
        String url = sc.nextLine();
        List<Post> posts = x.leerTodo(url);
        
        App.insertarPostBBDD(posts);
    }

    public static void insertarPostBBDD(List<Post> posts){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        try (Connection conexion = DriverManager.getConnection(b.connectionUrl)){
            PreparedStatement pst = conexion.prepareStatement("insert into post (titulo,pubDate,enlace,descripcion,contenido,ID_Usuario) values (?,?,?,?,?,?)");
            PreparedStatement comprobarPost = conexion.prepareStatement("select enlace from post where enlace = ?");


            for (Post p : posts){
                comprobarPost.setString(1, p.getEnlace());
                ResultSet rs = comprobarPost.executeQuery();
                if (!rs.next()){
                    pst.setString(1, p.getTitulo());
                    pst.setString(2, p.getPubDate().toString());
                    pst.setString(3, p.getEnlace());
                    pst.setString(4, p.getDescripcion());
                    pst.setString(5, p.getContenido());
                    pst.setInt(6, comprobarUsuario(p));
                    pst.executeUpdate();
                    insertarCategoriasPost(p);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }      

    }

    private static void insertarCategoriasPost(Post post){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        try (Connection conexion = DriverManager.getConnection(b.connectionUrl)){
            PreparedStatement insertarCategoriasPost = conexion.prepareStatement("insert into categoriasposts (ID_post,ID_Categoria) values (?,?)");
            PreparedStatement selectIdCategoria = conexion.prepareStatement("select id from post where enlace = ?");
            selectIdCategoria.setString(1, post.getEnlace());
            ResultSet rs = selectIdCategoria.executeQuery();
            rs.next();
            int idPost = rs.getInt(1);
            insertarCategoriasPost.setInt(1, idPost);
            for (int i=0;i <post.getCategorias().size();i++){
                int idCategoria = crearCategoria(post.getCategorias().get(i));
                insertarCategoriasPost.setInt(2, idCategoria);
                insertarCategoriasPost.executeUpdate();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static int crearCategoria(String nombre){
        int idCategoria;
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        try (Connection conexion = DriverManager.getConnection(b.connectionUrl)){
            PreparedStatement selectIdCategoria = conexion.prepareStatement("select id from categoria where nombre = ?");
            selectIdCategoria.setString(1, nombre);
            ResultSet rs = selectIdCategoria.executeQuery();

            if (rs.next()){
                idCategoria = rs.getInt(1);
            } else {
                PreparedStatement insertarCategoria = conexion.prepareStatement("insert into categoria (nombre) values (?)");
                insertarCategoria.setString(1, nombre);
                insertarCategoria.executeUpdate();
                rs = selectIdCategoria.executeQuery();
                rs.next();
                idCategoria = rs.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
            idCategoria = -1;
        }
        return idCategoria;
    }

    private static int comprobarUsuario(Post post){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        int idUsuario ;
        try (Connection conexion = DriverManager.getConnection(b.connectionUrl)){
            PreparedStatement comprobarUsuario = conexion.prepareStatement("select id from usuario where nombre = ?");
            PreparedStatement insertarUsuario = conexion.prepareStatement("insert into usuario (nombre) values (?)");

            comprobarUsuario.setString(1, post.getAutor());
            ResultSet rs =comprobarUsuario.executeQuery();

            if (rs.next()){
                idUsuario = rs.getInt(1);
            } else {
                insertarUsuario.setString(1, post.getAutor());
                insertarUsuario.executeUpdate();
                rs = comprobarUsuario.executeQuery();
                rs.next();
                idUsuario = rs.getInt(1);
            }

        }catch (Exception e){
            e.printStackTrace();
            idUsuario = -1;
        }
        


        return idUsuario;
    }

    protected static void CrearBBDD() {
        bbdd b = new bbdd(serverDB, portDB, "master", userDB, passwordDB);

        String i = b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='" + DBname + "' ");
        // String cero = "0";
        if (i.equals("0")) {
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b = new bbdd(serverDB, portDB, DBname, userDB, passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");

            b.modificarBBDD(query);

            b.modificarBBDD("create trigger asignarCategoria on Post for insert as declare @idPost as int = (select id from inserted) insert into CategoriasPosts (ID_Post,ID_Categoria) values (@idPost,1)");

        }

        
    }

    private static String LeerScriptBBDD(String archivo) {
        String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while ((cadena = b.readLine()) != null) {
                ret = ret + cadena;
            }
            b.close();
        } catch (Exception e) {
        }
        return ret;

    }
}

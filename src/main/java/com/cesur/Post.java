package com.cesur;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Post {
    private String titulo;
    private String enlace;
    private String contenido;
    private String descripcion;
    private LocalDateTime pubDate;
    private String autor;
    private int idUsuario;
    private List<String> categorias = new ArrayList<String>();

    public Post(String titulo, String enlace,String contenido, String descripcion, LocalDateTime pubDate, int idUsuario,
            List<String> categorias) {
        this.titulo = titulo;
        this.enlace = enlace;
        this.contenido = contenido;
        this.descripcion = descripcion;
        this.pubDate = pubDate;
        this.idUsuario = idUsuario;
        this.categorias = categorias;
    }



    @Override
    public String toString() {
        return "Post [contenido=" + contenido + ",\n descripcion=" + descripcion
                + ", \nenlace=" + enlace + ", \nidUsuario=" + idUsuario + ", \npubDate=" + pubDate + ", \ntitulo=" + titulo
                + "]";
    }


    public Post() {
    }

    

    public String getAutor() {
        return autor;
    }



    public void setAutor(String autor) {
        this.autor = autor;
    }



    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public List<String> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<String> categorias) {
        this.categorias = categorias;
    }

    public void addCategoria(String categoria) {
        this.categorias.add(categoria);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPubDate() {
        return pubDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
    }

    public void setPubDate(LocalDateTime pubDate) {
        
        this.pubDate = pubDate;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

}
/*
 * CREATE TABLE Post( ID int identity(1,1) primary key, titulo varchar(200) not
 * null, pubDate Date not null, enlace varchar(300) not null, descripcion
 * varchar(max) not null, ID_Usuario int not null, CONSTRAINT FK_ID_Usuario
 * FOREIGN KEY (ID_Usuario) REFERENCES Usuario(ID), )
 */